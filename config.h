#ifndef CONFIG_H
#define CONFIG_H

/*
 * Delay to have an answer by the server in seconds
 */
#define DELAIS 5

/*
 * Defined global variables to the rpc
 */
#define SOCK_PATH ".soquette"
#define BACK_LOG 100
#define BUFFER_SIZE 1024
#define MAX_ARGS 100

#endif // CONFIG_H
