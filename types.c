#include "types.h"

char* argtostring(const struct arg* a){
  /* Renvoi une serialisation de a.
   * Renvoie NULL si : a==NULL || a->type est inconnu
   */
  if(a==NULL)
    return NULL;
  switch(a->type){
  case TYPE_VOID : return type_void_to_string(a);
  case TYPE_INT : return type_int_to_string(a);
  case TYPE_STRING : return type_string_to_string(a);
  default : return NULL;
  }
}

char* type_int_to_string(const struct arg* a){
  /* Renvoi une chaine de caractere representant la struct arg en argument.
   * Retourne NULL si a est NULL ou a->type != TYPE_INT ou a->arg==NULL.
   * La chaine retournee finie par le caractere '\0'.
   */
  if(a==NULL || a->type!=TYPE_INT || a->arg==NULL)
    return NULL;
  // Get size of int
  int s_int =1, tmp = *((int*)(a->arg));
  if(tmp<0) // Pour les nombres negatif, compter le '-' au debut
    s_int++;
  while((tmp/=10)!=0)
    s_int++;
  // Creer la chaine et ecrit l'entier dedans
  char *str = malloc(sizeof(char) * (s_int + 3)); //size int + 0x01 + lg + '\0'
  str[0] = 0x01;
  str[1] = s_int;
  if(sprintf(&(str[2]), "%d", *((int*)(a->arg))) < 0)
    ERROR(1);
  str[s_int + 2] = '\0'; //Ajout caractere '\0' à la fin
  return str;
}

char* type_string_to_string(const struct arg* a){
  /* Renvoi une chaine de caractere representant la struct arg en argument.
   * Retourne NULL si a est NULL ou a->type != TYPE_STRING ou a->arg==NULL.
   * La chaine retournee finie par le caractere '\0'.
   */
  if(a==NULL || a->type!=TYPE_STRING || a->arg==NULL)
    return NULL;
  // Get string size (On supposera que a->arg fini par un caractere '\0'
  int s_str = strlen(a->arg); // Sans le dernier caractere '\0'
  char *str = malloc(sizeof(char) * (s_str + 3));
  str[0] = 0x02;
  str[1] = s_str;
  if(sprintf(&(str[2]),"%s", (char*)(a->arg)) < 0)
    ERROR(1);
  // a->arg fini par '\0', donc il a ete ajoute au moment du sprintf
  return str;  
}

char* type_void_to_string(const struct arg* a){
  /* Renvoi une chaine de caractere representant la struct arg en argument.
   * Retourne NULL si a est NULL ou a->type != TYPE_VOID.
   * La chaine retournee ne contient que le caractere 0x00.
   */
  if(a==NULL || a->type!=TYPE_VOID)
    return NULL;
  char * str = malloc(sizeof(char));
  str[0] = 0x00;
  return str;
}

struct arg * stringtoarg(char * c, int *next ){ // next est un indice
  /* Renvoi un type arg representant la premiere portion de la chaine c.
   * next permet de recuperer l'indice du debut de la partie suivante.
   *
   * Ex : c = "121024nbvc" (contient l'entier 10 et la chaine 'nbvc')
   *      a la fin de l'execution, on aura :
   *        c = "121024nbvc" , retour arg = {TYPE_INT , &(10)}
   *        next = 4 (autrement dit indiquera le debut de la chaine "24nbvc")
   *
   * c doit posseder l'arg complet pour ne pas bugguer
   * on supposera que c est correct (pas d'arnaque dans la chaine).
   */
  if(c == NULL)
    return NULL;
  if(c[0] == 0x00)
    return string_to_type_void(c,next);
  else if(c[0] == 0x01)
    return string_to_type_int(c,next);
  else if(c[0] == 0x02)
    return string_to_type_string(c,next);
  else
    return NULL;
}

struct arg * string_to_type_int(char * c, int *next){
  if(c==NULL)
    return NULL;
  if(c[0] != 0x01)
    return NULL;
  int size = c[1];
  char * nb = malloc(sizeof(char) * (size+1)); // +1 pour '\0' a la fin
  strncpy(nb,&(c[2]),size);
  nb[size] = '\0';
  int n = atoi(nb);
  free(nb); // plus besoin
  struct arg * a = malloc(sizeof(struct arg));
  a->type = TYPE_INT;
  a->arg = malloc(sizeof(int));
  *((int*)a->arg) = n;
  if(next != NULL)
    *next = size+2;
  return a;
}

struct arg * string_to_type_string(char * c, int *next ){
  if(c==NULL)
    return NULL;
  if(c[0] != 0x02)
    return NULL;
  int size = c[1];
  char * nb = malloc(sizeof(char) * (size+1)); // +1 pour '\0' a la fin
  strncpy(nb,&(c[2]),size);
  nb[size] = '\0';
  struct arg * a = malloc(sizeof(struct arg));
  a->type = TYPE_STRING;
  a->arg = nb;
  if(next != NULL)
    *next = size + 2;
  return a;
}

struct arg * string_to_type_void(char * c, int *next ){
  if(c==NULL)
    return NULL;
  if(c[0] != 0x00)
    return NULL;
  struct arg* a = malloc(sizeof(struct arg));
  a->type = TYPE_VOID;
  if(next != NULL)
    *next = 1;
  return a;
}
