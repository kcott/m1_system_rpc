#ifndef FUNCTION_H
#define FUNCTION_H

#include "error.h"
#include "config.h"
#include "types.h"

int call_function(char* function, int nargs, struct arg* args);

int plus(int i, int j);
void write_str(char* str);
int write_str2(char* str);
char* hello_world(void);

#endif // FUNCTION_H
