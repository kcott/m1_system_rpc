#ifndef TYPES_H
#define TYPES_H

#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include <string.h>


/*
 * Differents values returned by extern calls
 */
#define APPEL_OK          1
#define FONCTION_INCONNUE 2
#define MAUVAIS_ARGUMENTS 3
#define PAS_DE_REPONSE    4

typedef struct arg{
  int type;
  void* arg;
}arg;

typedef struct fun{
  char* name;
  int nb_arg;
  struct arg* arg;
}fun;

#define TYPE_VOID 1
#define TYPE_INT 2
#define TYPE_STRING 3

char* argtostring(const struct arg*);
char* type_int_to_string(const struct arg*);
char* type_string_to_string(const struct arg*);
char* type_void_to_string(const struct arg*);

struct arg * stringtoarg(char *, int *);
struct arg * string_to_type_int(char *, int *);
struct arg * string_to_type_string(char *, int *);
struct arg * string_to_type_void(char *, int *);

#endif // TYPES_H
