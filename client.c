#include "client.h"


void alarm_handler(int sig){
  if(sig==SIGALRM){
    printf("Error:no answer\n");
    ERROR(EXIT_FAILURE);
    close(fd_server);
  }
}

int appel_externe(const char* fonction,
		  unsigned short argc,
		  struct arg* argv){
  printf("Send request to function:%s\n",fonction);

  char str[BUFFER_SIZE];
  memset(str,0,BUFFER_SIZE);
  int length = strlen(fonction);
  //Ajout nom fonction
  strcpy(str, fonction);
  str[length++]= ' ';

  str[length++]= argc; //ajout du nombre d'argument

  int i;
  for(i=0; i<argc; i++){
    char * c = argtostring(&argv[i]);
    if(c[0] == 0x00)
      str[++length] = '\0';
    else {
      strcat(&(str[length]),c);
      length += strlen(c);
    }
    free(c);
  }

  if(write(fd_server, str, length+1) < 0){
    perror("Error write in server\n");
    ERROR(EXIT_FAILURE);
  }

  // Read result to server
  return read_result_server(argv);
}

void create_socket(int argc, char* argv[]){
  /*
   * Create the unix socket
   */
  int fd; // file descriptor
  if((fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0){
    perror("Error socket\n");
    ERROR(EXIT_FAILURE);
  }
  /*
   * Share the socket with a file
   */
  struct sockaddr_un *addr = malloc(sizeof(struct sockaddr_un));
  addr->sun_family = AF_UNIX;
  strncpy(addr->sun_path,
	  SOCK_PATH,
	  sizeof(addr->sun_path)-1);
  /*
   * Begin the connection on the socket
   */
  if(connect(fd, (struct sockaddr*) addr, sizeof(struct sockaddr_un)) < 0){
    perror("Error to connect socket\n");
    ERROR(EXIT_FAILURE);
  }
  if(dup2(fd, fd_server) < 0){ // fd_server is a global variable
    perror("Error to duplicate file descriptor to global file descriptor\n");
    ERROR(EXIT_FAILURE);
  }
  
  /*
   * Parse argv
   */
  struct fun* function = parse_argv(argc, argv);

  /*
   * Call extern function
   */
  int result = appel_externe(function->name,
			     function->nb_arg,
			     function->arg);
  if(result == APPEL_OK)
    display_result(function->arg);
}

void display_result(struct arg * a){
  if(a==NULL){
    perror("error : param is NULL\n");
    ERROR(EXIT_FAILURE);
  }
  switch(a->type){
  case TYPE_INT : printf("resultat = %d\n",*((int*)a->arg)); break;
  case TYPE_STRING : printf("resultat = %s\n", ((char*)a->arg)); break;
  case TYPE_VOID : printf("resultat = void\n"); break;
  }
}

int main(int argc, char *argv[]){
  if(argc == 2)
    if(strcmp(argv[1], "--help") || strcmp(argv[1], "-h")){
      manual();
      return EXIT_SUCCESS;
    }
  if(argc < 3){
    perror("Arguments not known");
    ERROR(EXIT_FAILURE);
  }
  
  // On prepare l'alarme
  if(signal(SIGALRM, alarm_handler) == SIG_ERR){
    perror("Error:put alarm\n");
    ERROR(EXIT_FAILURE);
  }
  
  create_socket(argc, argv);
  
  close(fd_server);
  
  return EXIT_SUCCESS;
}

void manual(void){
  printf("NAME\n");
  printf("\tSend a function to the server in the same host\n");
  printf("UTILISATION\n");
  printf("\tclient [function type [argi ...]]\n\n");
  printf("\tstart a client if the server is launch\n");
  printf("\t\tfunction\tis the name of remote function\n");
  printf("\t\ttype\t\tis the return type of the function\n");
  printf("\t\targi\t\tare arguments to the function\n");
  printf("DESCRIPTION\n");
  printf("\tThis client can send three c types to the remote server\n");
  printf("\t\tvoid\t\tonly to the return type\n");
  printf("\t\tint\t\t\n");
  printf("\t\tstring\t\t\n");
}

struct fun* parse_argv(int size, char* argv[]){
  struct fun* f = malloc(sizeof(struct fun));
  f->name = argv[1];
  f->nb_arg = size - 2;
  struct arg* args = malloc(sizeof(struct arg) * (size -2));
  // args contient le retour en position 0
  args[0] = *return_string_to_arg(argv[2]);
  int i;
  for(i=3; i<size; i++){
    args[i-2] = *string_to_arg(argv[i]);
  }
  
  f->arg = args;
  return f;
}

int read_result_server(struct arg* ret){
  printf("Waiting server answer...\n");
  char buffer = 0;
  ssize_t bytes;

  // On lance l'alarme
  alarm(5);
  
  // On recupere d'abord l'etat de sortie de la fonction appelee
  if((bytes = read(fd_server, &buffer, 1)) < 0){
    perror("Error read socket\n");
    ERROR(EXIT_FAILURE);
  }

  // On desactive l'alarme
  alarm(0);

  if(buffer == APPEL_OK){
    if(ret!=NULL)
      recup_arg(ret);
  } else if(buffer == FONCTION_INCONNUE) {
    printf("Error:unknown function\n");
  } else if(buffer == MAUVAIS_ARGUMENTS) {
    printf("Error:bad args to call function\n");
  } else if(buffer == PAS_DE_REPONSE) {
    printf("Error:no answer\n");
    ERROR(EXIT_FAILURE);
  } else {
    printf("Error:unknown answer server:%d\n",buffer);
  }
  return (int)buffer;
}

void recup_arg(struct arg * ret){
  char buffer [BUFFER_SIZE];
  ssize_t bytes;
  if((bytes = read(fd_server, buffer, BUFFER_SIZE)) < 0){
    perror("Error read socket\n");
    ERROR(EXIT_FAILURE);
  }
  // On recupere la struct arg envoyee dans la socket
  int * i = malloc(sizeof(int));
  struct arg * a = stringtoarg(buffer, i);
  if(ret != NULL){
    //verifie que le type retourne correspond a celui attendu
    if(ret->type != a->type){
      // Assert false
      printf("Error:type returned incorrect\n");
      printf("\texpected %d\n\treceived%d)\n",ret->type,a->type);
      ERROR(0);
    }
    //On place ensuite le retour dans l'arg en parametre
    ret->type = a->type;
    ret->arg  = a->arg;
  }
  free(a);
}

struct arg* return_string_to_arg(char type[]){
  struct arg* a = malloc(sizeof(struct arg));
  if(strcmp(type, "void") == 0){
    a->type = TYPE_VOID;
  }else if(strcmp(type, "int") == 0){
    a->type = TYPE_INT;
    a->arg  = malloc(sizeof(int));
    *((int*) a->arg) = 0;
  }else if(strcmp(type, "string") == 0){
    a->type = TYPE_STRING;
    a->arg  = malloc(sizeof(char));
    *((char*) a->arg) = '\0';
  }else{
    perror("Type ");
    perror(type);
    perror(" not found\n");
    ERROR(EXIT_FAILURE);
  }
  return a;
}

// Deprecated
void send_arg(struct arg arg){
  char * str = argtostring(&arg);
  int size = strlen(str);
  if(arg.type == TYPE_VOID) {
    if(write(fd_server, str, 1) < 0){
      perror("Error write in server\n");
      ERROR(EXIT_FAILURE);
    }
  } else if(arg.type == TYPE_INT){
    if(write(fd_server, str, size-1) < 0){
      perror("Error write in server\n");
      ERROR(EXIT_FAILURE);
    }
  }else if(arg.type == TYPE_STRING){
    if(write(fd_server, str, size-1) < 0){
      perror("Error write in server\n");
      ERROR(EXIT_FAILURE);
    }
  }
  if(str != NULL)
    free(str);
}

struct arg* string_to_arg(char type[]){
  struct arg* a = malloc(sizeof(struct arg));
  int n = atoi(type);
  char s[strlen(type)];
  sprintf(s, "%d", n);
  if(strcmp(s, type) == 0){
    // Is a int
    a->type = TYPE_INT;
    a->arg = malloc(sizeof(int));
    *((int*)a->arg) = n;
  }else{
    // Is not a int
    a->type = TYPE_STRING;
    a->arg = type;
  }
  return a;
}
