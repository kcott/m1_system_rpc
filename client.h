#ifndef CLIENT_H
#define CLIENT_H

#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>

#include "config.h"
#include "error.h"
#include "types.h"

int fd_server;

void alarm_handler(int);
int appel_externe(const char* fonction,
		  unsigned short argc,
		  struct arg* argv);
void display_result(struct arg *);
void create_socket(int argc, char* argv[]);
int main(int argc, char *argv[]);
void manual(void);
struct fun* parse_argv(int size, char* argv[]);
int read_result_server(struct arg *);
void recup_arg(struct arg *);
struct arg* return_string_to_arg(char type[]);
// Deprecated
void send_arg(struct arg arg);
struct arg* string_to_arg(char type[]);

#endif // CLIENT_H
