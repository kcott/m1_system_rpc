FLAGS = -Wall

all: project

project: error.h types.h client.c serveur.c
	gcc $(FLAGS) -c -o error.o error.h
	gcc $(FLAGS) -c -o config.o config.h
	gcc $(FLAGS) -c -o types.o types.c
	gcc $(FLAGS) -c -o function.o function.c
	gcc $(FLAGS) -o client client.c types.o
	gcc $(FLAGS) -pthread -o serveur serveur.c types.o function.o

clean:
	rm -f *~
	rm -f *.o
	rm -f client serveur
