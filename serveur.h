#ifndef SERVEUR_FILE
#define SERVEUR_FILE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <signal.h>
#include <memory.h>
#include <fcntl.h>
#include <pthread.h>

#include "types.h"
#include "config.h"
#include "function.h"

#define SIZE_BUFFER 1024

void *read_request(void* arguments);

#endif
