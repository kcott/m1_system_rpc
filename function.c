#include "function.h"

// nargs = arguments number add with return
int call_function(char* function, int nargs, struct arg* args){
  if(args[0].type == TYPE_INT
     && strcmp(function, "plus") == 0){
    if(nargs != 3)
      return MAUVAIS_ARGUMENTS;
    if(args[1].type != TYPE_INT
       || args[2].type != TYPE_INT)
      return MAUVAIS_ARGUMENTS;
    args[0].arg = malloc(sizeof(int)*1);
    *(int*)args[0].arg = plus(*(int*)args[1].arg, *(int*)args[2].arg);
    return APPEL_OK;
  }else if(args[0].type == TYPE_VOID
     && strcmp(function, "write") == 0){
    if(nargs != 2)
      return MAUVAIS_ARGUMENTS;
    write_str((char*)args[1].arg);
    return APPEL_OK;
  } else if(args[0].type == TYPE_INT
     && strcmp(function, "write") == 0){
    if(nargs != 2)
      return MAUVAIS_ARGUMENTS;
    *(int*)args[0].arg = write_str2((char*)args[1].arg);
    return APPEL_OK;
  } else if(args[0].type == TYPE_STRING
     && strcmp(function, "hello") == 0){
    if(nargs != 1)
      return MAUVAIS_ARGUMENTS;
    args[0].arg = hello_world();
    return APPEL_OK;
  }
  return FONCTION_INCONNUE;
}

int plus(int i, int j) {
  return i+j;
}

void write_str(char* str){
  printf("%s\n",str);
}

int write_str2(char* str){
  printf("%s\n",str);
  return 0;
}

char* hello_world(void){
  return "Hello World\n";
}
