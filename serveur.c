#include "serveur.h"

#include <stdarg.h>

void exit_error(char *str, ...) {
	va_list args;
        va_start(args, str);
        char buffer[1024];
        sprintf(buffer, str, args);
        perror(buffer);
        va_end(args);
        exit(EXIT_FAILURE);
}

static void hdl (int sig) {
        remove(SOCK_PATH);
        exit(0);
}

void write_answer(int fd, int state, struct arg* answer) {
        char str[SIZE_BUFFER];
        memset(str,0,SIZE_BUFFER);
        str[0] = state;
	int length = 1;
	char * c;
	
	if(answer->type==TYPE_VOID)
	  length++;
	else if(answer->arg != NULL){
	  c = argtostring(answer);
	  length = strlen(c) +1;
	  strcat(&(str[1]), c);
	}
	
	free(c);

	if(send(fd, str, length, MSG_NOSIGNAL) <= 0)
		perror("Write error on socket\n");
}

void *read_request(void* arguments) {
        printf("Connection etablished\n");
	int fd = *(int*)arguments;
        char buffer[SIZE_BUFFER];
	memset(buffer, 0, SIZE_BUFFER);
        int n, i;
	int state = APPEL_OK;
        if( (n = read(fd, buffer, SIZE_BUFFER - 1)) < 0)
		exit_error("Read error\n");
        printf("%d bytes read\n", n);
        if(n == 0)
	        return NULL;
	
	// Get the lenght of name of function
        for(i=0; i<n; i++)
          if(buffer[i] == ' ')
            break;
	// Init name of function
        char name[i+1];
        strncpy(name, buffer, i);
        name[i] = '\0'; // char NULL to the end of char*
	i++; // Jump char ' '

	//recuperation des arguments
	int nargs = buffer[i++]; // nb argument a lire
	int * ind = malloc(sizeof(int));
	struct arg * args = malloc(sizeof(struct arg)*nargs);
	n=0;
	while(n < nargs){
	        args[n++] = *stringtoarg(buffer+i, ind);
		i += *ind;
	}
	state = call_function(name, nargs, args);
	
        fflush(stdout);
        write_answer(fd, state, args);
	free(args);
	return 0;
}

int main (int argc, char *argv[]) {
        int sock, fd;
        struct sockaddr_un serv, client;
        socklen_t fclient;
        char soquette[] = SOCK_PATH;
	
	//signals for server interruption
	struct sigaction act;
	memset (&act, 0, sizeof(act));
	act.sa_handler = hdl;
	int i;
	for(i=1; i<65; ++i)
		if (sigaction(i, &act, NULL));
	
	sock = socket(AF_LOCAL, SOCK_STREAM, 0);
	if(sock < 0) exit_error("socket error");

	memset((char *) &serv, 0, sizeof(serv));
	serv.sun_family = AF_LOCAL;
	strcpy(serv.sun_path, soquette);

	//here max 5 connections
	if(bind(sock, (struct sockaddr *) &serv, sizeof(serv)) < 0)
		exit_error("bind error");
	if(listen(sock, 5) <0)
		exit_error("listen error");
	fclient = sizeof(client);
	printf("Server ready\n");

	//infinite loop for read & write
	pthread_t t;
	for( ; ; ) {
		fd = accept(sock, (struct sockaddr *)&client, &fclient);
		if(fd < 0)
			exit_error("accept error");
		//int * pfd = &fd;
		pthread_create(&t, NULL, read_request, (void*) &fd);
		pthread_join(t, NULL);
		//printf("Connection etablished\n");
		//read_request(fd);
	}

	return EXIT_SUCCESS;
}

