#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define ERROR(end) do{ \
  fprintf( \
    stderr, \
    "error in file %s in line %d\n%s\n", __FILE__, __LINE__, \
    strerror(errno)); \
  if(end > 0) exit(end); \
}while(0)
#endif // ERROR_H
